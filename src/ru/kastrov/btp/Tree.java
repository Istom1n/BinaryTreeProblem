package ru.kastrov.btp;

import java.util.LinkedList;
import java.util.Queue;

public class Tree {
    private Node rootNode = null;  // Root node
    private int amount = 0;        // Node amount

    Tree() { }

    Tree(int data) {
        rootNode = new Node(amount, data);

        amount += 1;
    }

    public void addNode(int data) {
        if (rootNode == null) {
            rootNode = new Node(amount, data);

            amount += 1;
        } else {
            Queue<Node> queue = new LinkedList<>();

            queue.add(rootNode);

            while (!queue.isEmpty()) {
                Node node = queue.remove();

                if (node.id == (amount - 1) / 2) {
                    if (node.left == null) {
                        node.left = new Node(amount, data);
                    } else {
                        node.right = new Node(amount, data);
                    }

                    amount += 1;
                    return;
                }

                if (node.left != null) {
                    queue.add(node.left);
                }

                if (node.right != null) {
                    queue.add(node.right);
                }
            }
        }
    }

    public void removeNode(int id) {
        if (amount < id) {
            System.out.format("Node with id: %d isn't in tree", id);
        } else {
            Queue<Node> queue = new LinkedList<>();

            queue.add(rootNode);

            while (!queue.isEmpty()) {
                Node node = queue.remove();

                if (node.id == id / 2) { // Parent node of this id
                    if (node.id % 2 == 1) { // Which way (left odd, right even)
                        node.left = null;
                    } else {
                        node.right = null;
                    }

                    amount -= 1;
                    return;
                }

                if (node.left != null) {
                    queue.add(node.left);
                }

                if (node.right != null) {
                    queue.add(node.right);
                }
            }
        }
    }

    // Just make BFS for print
    public void showTree() {
        Queue<Node> queue = new LinkedList<>();

        if (rootNode != null) {
            queue.clear();
            queue.add(rootNode);

            while (!queue.isEmpty()) {
                Node node = queue.remove();
                System.out.print(node.data + " ");

                if (node.left != null) queue.add(node.left);
                if (node.right != null) queue.add(node.right);
            }
        }
    }
}
