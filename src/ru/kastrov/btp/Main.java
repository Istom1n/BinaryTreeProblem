package ru.kastrov.btp;

public class Main {

    public static void main(String[] args) {
        Tree tree = new Tree(34);

        tree.addNode(120); // 0
        tree.addNode(23);  // 1
        tree.addNode(44);  // 2
        tree.addNode(224); // 3
        tree.addNode(464); // 4
        tree.addNode(35);  // 5
        tree.addNode(77);  // 6
        tree.addNode(55);  // 7

        System.out.println("Add some elements:");
        tree.showTree();

        tree.removeNode(2);

        System.out.println("\nAfter delete:");
        tree.showTree();
    }
}
