package ru.kastrov.btp;

public class Node {
    int id;
    int data;

    Node left;
    Node right;

    Node (int id, int data) {
        this.id = id;
        this.data = data;
        left = right = null;
    }

}
